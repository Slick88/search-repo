import Vue from "vue";
import Router from "vue-router";
import Landing from "./views/Landing";
import Display from "./views/Display";
import ReadmeReader from "./views/ReadmeReader";

Vue.use(Router);

export default new Router({
  routes: [
    {
      path: "/",
      name: "landing",
      component: Landing
    },
    {
      path: "/display/:username",
      name: "display",
      props: true,
      component: Display
    },
    {
      path: "/readmereader/:username/:repo",
      name: "readmereader",
      props: true,
      component: ReadmeReader
    }
  ],
  mode: "history"
});
