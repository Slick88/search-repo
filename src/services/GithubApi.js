import axios from "axios";

export default {
  fetchUserRepos(id) {
    return axios
      .get(`https://api.github.com/users/${id}/repos`)
      .then(response => {
        return response.data;
      });
  },

  fetchReadME(owner, repo) {
    const headers = { Accept: "application/vnd.github.VERSION.html+json" };
    return axios
      .get(`https://api.github.com/repos/${owner}/${repo}/readme`, {
        headers
      })
      .then(response => {
        return response.data;
      });
  }
};
